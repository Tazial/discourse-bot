import discord
import yaml
import re

# Notes:
#   Needed perms are read and react for the discussions channel, and manage roles and channels for global perms

# Initialize some stuff
config = None
with open('config.yml') as config_file:
	config = yaml.load(config_file)
client = discord.Client()

# Parse the config
token = config.get('token')
channels = dict((chnl['channel-id'], chnl['category-id']) for chnl in config.get('channels', [])) # A dict of the channel id to the category id that discussion threads should be created in

@client.event
async def on_ready():
	print(f'Logged in as {client.user}')
# Discord.py doesn't seem to like perms atm, so this is disabled for now
# 	bad_channels = []
# 	for channel_id, category_id in channels.items():
# 		channel = client.get_channel(channel_id)
# 		category = client.get_channel(category_id)
# 		if not channel.permissions_for(client.user).read_messages:
# 			try:
# 				await channel.set_permissions(client.user, read_messages=True)
# 				print('Warn: Missing permission to read messages in channel {channel} ({channel.id}), added permission overwrite')
# 			except discord.errors.Forbidden:
# 				print('ERROR: Missing permission to read messages in channel {channel} ({channel.id}) and don\'t have permissions to fix this.  This channel will not be tracked.')
# 				bad_channels.append(channel_id)
# 				continue
# 		category_perms = category.permissions_for(client.user)
# 		if not category_perms.read_messages and category_perms.send_messages:
# 			try:
# 				await category.set_permissions(client.user, read_messages=True, send_messages=True)
# 				print('Warn: Missing permission to read and/or send messages in category {category} ({category.id}), added permission overwrite')
# 			except discord.errors.Forbidden:
# 				print('ERROR: Missing permission to read and/or send messages in category {category} ({category.id}) and don\'t have permissions to fix this.  This channel will not be tracked.')
# 				bad_channels.append(channel_id)
# 				continue
# 	for bad_channel in bad_channels:
# 		del channels[bad_channel]
			
@client.event
async def on_message(message):	
	if message.channel.id in channels.keys():
		await message.add_reaction('👍')
		await message.add_reaction('👎')
		await message.add_reaction('💬')
		await get_or_make_channel(message)
	if message.type == discord.MessageType.pins_add and message.channel.category_id in channels.values(): # Delete any pin messages in discussion channels (normally just the original pin)
		await message.delete()

async def get_or_make_channel(message):
	category_id = channels[message.channel.id]
	discussion = next((channel for channel in message.guild.channels if str(message.id) in channel.name and channel.category and channel.category.id == category_id), None)
	if not discussion:
		category = next((channel for channel in message.channel.guild.channels if channel.id == category_id), None)
		if category:
			clean_name = re.sub(r'[^a-zA-Z]+','-',message.content)[:23].lower().strip('-')
			discussion = await message.channel.guild.create_text_channel(f'{clean_name}-{message.id}', category=category)
			await discussion.set_permissions(client.user, read_messages=True, send_messages=True, manage_messages=True)
			embed = discord.Embed(description=message.content)
			embed.set_author(name='@%s'%message.author,icon_url=message.author.avatar_url_as(format='png'))
			header_message = await discussion.send(embed=embed)
			await header_message.pin()
			print(f'Channel created for message "{message.content}" ({message.id})')
		else:
			print('Warning: Category with id {category_id} not found in guild {message.channel.guild.name} ({message.channel.guild.id}).  Not creating discussion channel.')
	return discussion
	

@client.event
async def on_raw_reaction_add(reaction):
	await on_reaction(reaction, True)
	if reaction.emoji.name in '👍👎' and reaction.user_id != client.user.id and reaction.channel_id in channels.keys():
		other_vote = '👎' if reaction.emoji.name == '👍' else '👍'
		message = await client.get_channel(reaction.channel_id).get_message(reaction.message_id)
		other_reaction = next((reaction for reaction in message.reactions if reaction.emoji == other_vote), None)
		if other_reaction:
			async for user in other_reaction.users():
				if user.id == reaction.user_id:
					await message.remove_reaction(other_reaction.emoji, user)
					break

@client.event
async def on_raw_reaction_remove(reaction):
	await on_reaction(reaction, False)

async def on_reaction(reaction, add):
	if reaction.emoji.name == '💬' and reaction.user_id != client.user.id and reaction.channel_id in channels.keys():
		guild = client.get_guild(reaction.guild_id)
		message = await client.get_channel(reaction.channel_id).get_message(reaction.message_id)
		channel = await get_or_make_channel(message)
		user = guild.get_member(reaction.user_id)
		await channel.set_permissions(user, read_messages=add, reason='Automated discussion channel by DiscourseBot')
		print(f'{user} {"added to" if add else "removed from"} discussion channel {channel}')
		if add: # Ping the user in the new channel to get their attention
			mention = await channel.send(user.mention)
			await mention.delete()

@client.event
async def on_raw_message_delete(message):
	if message.channel_id in channels.keys():
		guild = client.get_guild(message.guild_id)
		channel = next((channel for channel in guild.channels if str(message.message_id) in channel.name and channel.category and channel.category.id == channels[message.channel_id]), None)
		if channel:
			print(f'Deleting channel {channel} becuase the corresponding message was deleted')
			await channel.delete(reason='Message corresponding to this channel was deleted')
		else:
			print('Warning: Aparently untracked messaged deleted in tracked channel')

client.run(config['token'])
